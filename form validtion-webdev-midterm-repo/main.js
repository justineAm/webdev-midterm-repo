// Wait for the DOM to be ready
$(function () {
   
    $("form[name='registration']").validate({
        rules: {

            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
            phoneNumber: {
                minlength: 13,
                maxlength: 13
            },
            password: {
                required: true,
                minlength: 8
            },
            birthdate: {
                required: true,

            }
        },
        messages: {

            password: {
                required: "Please provide a password",

            },
            email: "Please enter a valid email "
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
            alert('Account created Successfully')
        }
    });
    $('#firstname').keyup(function () {

        var firstname = $('#firstname').val();
        if (firstname) { // both fields are filled, let's validate

            //check if either value contains any numbers
            if (/[0-9]/.test(firstname)) {
                alert('Please input a valid Firstname');
                return false
               

            }
            //check if either value contains any characters besides words and hyphens
            else if (!/^[A-z/-/./ /]+$/.test(firstname)) {
                alert('Please input a valid Firstname');
                return false
               
            }

        } else {

            return true
            


        }
    })
    //onclick button if the alert 
    $('#btn').click(function () {
        var firstname = $('#firstname').val();
        if (firstname == "") {
            alert('Please input a valid Firstname');
            return false
           
        }
        if (firstname) { // both fields are filled, let's validate

            //check if either value contains any numbers
            if (/[0-9]/.test(firstname)) {
                alert('Please input a valid Firstname');
                return false
               

            }
            //check if either value contains any characters besides words and hyphens
            else if (!/[A-z/-/ /.]+$/.test(firstname)) {
                alert('Please input a valid Firstname');
                return false
               
            }

        } else {

            return true
            



        }
    })


    //Lastname validator
    $('#lastname').keyup(function () {
        var lastname = $('#lastname').val();

        if (lastname) { // both fields are filled, let's validate

            //check if either value contains any numbers
            if (/[0-9]/.test(lastname)) {
                alert('Please input a valid Lastname');
               
            }
            //check if either value contains any characters besides words and hyphens
            else if ((!/[A-z/-/ /.]+$/.test(lastname))) {
                alert('Please input a valid Lastname');
                return false
               
            }

        } else {
            return true
            
        }
    })

    $('#btn').click(function () {
        var lastname = $('#lastname').val();
        if (lastname == "") {
            alert('Please input a valid Lastname');
            return false
           

        }
        if (lastname) { // both fields are filled, let's validate

            //check if either value contains any numbers
            if (/[0-9]/.test(lastname)) {
                alert('Please input a valid Lastname');
               
            }
            //check if either value contains any characters besides words and hyphens
            else if ((!/^[A-z/-/ /.]+$/.test(lastname))) {
                alert('Please input a valid Lastname');
               
            }

        } else {
            return true
            
        }
    })


    //birthdate validation
    $('#btn').click(function () {
        var mydate = $("#birthdate").val();
        var bodslice = (Number(mydate.slice(0, 4)));
        var myAge = 2021 - (Number(bodslice))
        if (myAge < 18) {
            alert('You must be 18 years old and above to create an account.')
            return false
        } else {
            return true
            
        }


    })
    //end birthdate validation


    //phone number validator
    $('#phoneNumber').val('+63');



    $('#btn').click(function () {
        var phoneNumber = $('#phoneNumber').val();
        var phoneRegex = /^[0-9-+]+$/;

        if (phoneNumber == "") {
            alert('Please input a valid Phone Number');
           

        }
        var slicedNum = phoneNumber.slice(0, 3)
        if (phoneNumber) {
            if (/[A-z/]/.test(phoneNumber)) {
                alert("Please enter a valid Phone Number");
                return false
            } 
            if (slicedNum !='+63') {
                alert("Your phone number is not on PH origin. Accounts are exclusive for Philippine users only");
                return false
            }
            if (phoneNumber.match(phoneRegex)) {
                return true
                
            } else {
                alert('Please input a valid Phone Number');
                return false
               


            }
        }


    })
    //end of phone validator

    //Email validator
    $('#btn').click(function () {
        var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
        var email = $("#email").val();
        if (email.match(regMail) == null) {
            alert("Please enter a valid email");
            return false
           

        }

    })

    //end of email validator


    
    // password validator

    $('#btn').click(function () {
        var pass1 = $('#password').val();
        var pass2 = $('#password1').val();
        var validatePass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
        if (pass2 != pass1) {
            alert('Your passwords do not match. Please try again')
            return false
           
        }
        if (pass1.match(validatePass) == null) {
            alert('Your password must have 8 characters or more, and contain at least one special character, number, uppercase letter, and lowercase letter.')
            return false
           
        } else {
            return true
           
        }
        
    })
    //end of validator
    //show password
    $(document).ready(function () {
        $("#toggle").change(function () {

            // Check the checkbox state
            if ($(this).is(':checked')) {
                // Changing type attribute
                $("#password").attr("type", "text");

                // Change the Text
                $("#toggleText").text("Hide");
            } else {
                // Changing type attribute
                $("#password").attr("type", "password");

                // Change the Text
                $("#toggleText").text("Show");
            }

        });
    });
    $(document).ready(function () {
        $("#toggle").change(function () {

            // Check the checkbox state
            if ($(this).is(':checked')) {
                // Changing type attribute
                $("#password1").attr("type", "text");

                // Change the Text
                $("#toggleText").text("Hide");
            } else {
                // Changing type attribute
                $("#password1").attr("type", "password");

                // Change the Text
                $("#toggleText").text("Show");
            }

        });
    });

    //end of password show 
    $('#btn').click(function () {
        if (areTrueth == true) {
            alert('Account Created Successfully')
        }
    })


});
